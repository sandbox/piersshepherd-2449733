<?php

/**
 * Implements hook_field_formatter_info().
 */
function bootstrap_image_formatter_field_formatter_info() {
  return array(
    // The key must be unique, so it's best to prefix with your module's name.
    'bootstrap_image_formatter_bootstrap' => array(
      // The label is is what is displayed in the select box in the UI.
      'label' => t('Bootstrap'),
      // Field types is the important bit!! List the field types your formatter is for.
      'field types' => array('image'),
      // An array of settings with default values.
      'settings' => array(
        'image_style' => '',
        'responsive' => '',
        'rounded' => '',
        'circle' => '',
        'thumbnail' => '',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function bootstrap_image_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  // This gets the view_mode where our settings are stored
  $display = $instance['display'][$view_mode];
  // This gets the actual settings
  $settings = $display['settings'];
  // Initialize the element variable
  $element = array();
  // Add option fields.
  $element['image_style'] = array(
    '#type'           => 'select',
    '#title'          => t('Image style'),
    '#description'    => t('Select an image style'),
    '#default_value'  => $settings['image_style'],
    '#options'        => image_style_options($include_empty = TRUE, $output = CHECK_PLAIN),
  );
  
  $element['responsive'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Responsive?'),
    '#description'    => t('Add responsive bootstrap classes'),
    '#default_value'  => $settings['responsive'],
  );

  $element['rounded'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Rounded?'),
    '#description'    => t('Add roudned image bootstrap classes'),
    '#default_value'  => $settings['rounded'],
  );

  $element['circle'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Circle?'),
    '#description'    => t('Add circle image bootstrap classes'),
    '#default_value'  => $settings['circle'],
  );

  $element['thumbnail'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Thumbnail?'),
    '#description'    => t('Add thumbnail bootstrap classes'),
    '#default_value'  => $settings['thumbnail'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function bootstrap_image_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = t('Image style: @size<br>', array(
    '@size' => $settings['image_style'],
  ));

  $responsive = ($settings['responsive'] == 1 ? 'Yes' : 'No');
  $summary .= t('Responsive') . ': ' . t('@responsive', array('@responsive' => $responsive)) . '<br>';
  
  $rounded = ($settings['rounded'] == 1 ? 'Yes' : 'No');
  $summary .= t('Rounded') . ': ' . t('@rounded', array('@rounded' => $rounded)) . '<br>';
  
  $circle = ($settings['circle'] == 1 ? 'Yes' : 'No');
  $summary .= t('Circle') . ': ' . t('@circle', array('@circle' => $circle)) . '<br>';
  
  $thumbnail = ($settings['thumbnail'] == 1 ? 'Yes' : 'No');
  $summary .= t('Thumbnail') . ': ' . t('@thumbnail', array('@thumbnail' => $thumbnail)) . '<br>';


  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 * This code just passes straight through to a theme function. The theme function
 * name we call is $display['type'].
 */
function bootstrap_image_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $elements = array();
  foreach ($items as $delta => $item) {
    $elements[$delta] = array(
      '#markup' => theme($display['type'], array(
        'element' => $item,
        'field' => $instance,
        'style' => $display['settings']['image_style'],
        'responsive' => $display['settings']['responsive'],
        'rounded' => $display['settings']['rounded'],
        'circle' => $display['settings']['circle'],
        'thumbnail' => $display['settings']['thumbnail'],
      )
      ),
    );
  }
  return $elements;
}

/**
 * Implements hook_theme().
 */
function bootstrap_image_formatter_theme() {
  return array(
    // The theme function that our formatter uses:
    'bootstrap_image_formatter_bootstrap' => array(
      // Don't forget that all Drupal 7 theme functions have only one argument,
      // so you declare what variables get passed within that argument instead.
      // See http://drupal.org/node/224333#theme_changes
      'variables' => array('element' => NULL),
    ),
  );
}

/**
 * Theme function for 'bootstrap_image_formatter_absolute_url' link field formatter.
 */
function theme_bootstrap_image_formatter_bootstrap($element) {
  // Array of settings ready for theme_image_style().
  $variables = array(
    'style_name' => $element['style'],
    'path' => $element['element']['uri'],
    'width' => $element['element']['width'],
    'height' => $element['element']['height'],
    'alt' => isset($element['element']['field_file_image_alt_text']) ? $element['element']['field_file_image_alt_text'] : "",
    'title' => isset($element['element']['field_file_image_title_text']) ? $element['element']['field_file_image_title_text'] : "",
    'attributes' => array(
      'class' => array(),
    ),
  );

  // If values are present, append them to the class array.
  if ($element['responsive'] == 1) $variables['attributes']['class'][] = 'img-responsive';
  if ($element['rounded'] == 1) $variables['attributes']['class'][] = 'img-rounded';
  if ($element['circle'] == 1) $variables['attributes']['class'][] = 'img-circle';
  if ($element['thumbnail'] == 1) $variables['attributes']['class'][] = 'img-thumbnail';

  // Theme the image.
  $image = theme_image_style($variables);
  
  // Return the image markup.
  return $image;
}

